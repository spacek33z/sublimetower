SublimeSourceTree
=================

Very simple plugin to open [Tower](http://www.git-tower.com/) from [Sublime Text](http://www.sublimetext.com) 2 / 3.

Based on [sublimesourcetree](https://bitbucket.org/PhillSparks/sublimesourcetree).

Installing
----------

**Using Git:** Clone the repository in your Sublime Text 2 Packages directory and restart Sublime Text 2:

    git clone git@bitbucket.org:spacek33z/sublimetower.git

Usage
-----

Open Tower and install the command line tool by going to `Preferences` -> `Integration`.

Open the command palette and execute the ``Tower: Open Tower`` command to open the repository in which the currently opened file is located.

Sample user key binding to execute the command::

    { "keys": ["super+."], "command": "tower_open" }

Configuration
-------------

Additional settings can be configured in the User File Settings:

`tower_path`: the path to the `stree` executable (default: `"/usr/local/bin/stree"`)

Changelog
---------
v1.0 (03-05-2015):

* Initial version

License
-------
See the LICENSE-MIT.txt file.
