import locale
import os
import subprocess
import sublime
import sublime_plugin


class TowerOpenCommand(sublime_plugin.WindowCommand):
    def is_enabled(self):
        return True

    def get_path(self):
        if self.window.active_view():
            return self.window.active_view().file_name()
        elif self.window.folders():
            return self.window.folders()[0]
        else:
            sublime.status_message(__name__ + ': No place to open Tower to')
            return False

    def run(self, *args):
        sublime.status_message(__name__ + ': running')
        path = self.get_path()
        if not path:
            sublime.status_message(__name__ + ': No path')
            return False
        if os.path.isfile(path):
            path = os.path.dirname(path)

        settings = sublime.load_settings('Base File.sublime-settings')
        tower_path = settings.get('gittower_path', '/usr/local/bin/gittower')

        if not os.path.isfile(tower_path):
            mac_path = '/Applications/Tower.app'
            if os.path.isdir(mac_path):
                tower_path = mac_path
            else:
                tower_path = None

        if tower_path in ['', None]:
            sublime.error_message(__name__ + ': tower executable path not set, incorrect or no gittower?')
            return False

        if tower_path.endswith(".app"):
            subprocess.call(['open', '-a', tower_path, path])
        else:
            p = subprocess.Popen([tower_path, path.encode(locale.getpreferredencoding(do_setlocale=True))])


class TowerOpenProjectCommand(TowerOpenCommand):
    def get_path(self):
        if self.window.folders():
            return self.window.folders()[0]
        else:
            sublime.status_message(__name__ + ': No place to open Tower to')
            return False
